package assignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Driver;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.DataProvider;

public class AutomationScripts {

	public void launchApplication(WebDriver driver) {
		
		driver.get("https://admin-demo.nopcommerce.com/login");
	}
	
	public String loginToApplication(WebDriver driver) {
		WebElement email = driver.findElement(By.id("Email"));
		email.clear();
		email.sendKeys("admin@yourstore.com");
		WebElement password = driver.findElement(By.id("Password"));
		password.clear();
		password.sendKeys("admin");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String name=driver.findElement(By.xpath("//a[@class='nav-link disabled']")).getText();
		return name;
	}
	
	public String searchProduct(WebDriver driver) {
		
		driver.findElement(By.linkText("Catalog")).click();
		driver.findElement(By.linkText("Products")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		WebElement produ = driver.findElement(By.id("SearchCategoryId"));
		Select sel = new Select(produ);
		sel.selectByVisibleText("Computers >> Desktops");
		
		driver.findElement(By.id("search-products")).click();
		
		String product = driver.findElement(By.xpath("//tr[@class='odd']/td[3]")).getText();
		return product;
	}
	
	 
	 public String categories(WebDriver driver) throws InterruptedException, IOException {
		 
		 //driver.findElement(By.linkText("Catalog")).click();
		 
		 driver.findElement(By.linkText("Categories")).click();
		 driver.findElement(By.partialLinkText("Add new")).click();
		 File f = new File("/home/shrikanth/Documents/Test_data/Assignment.xlsx");
		 FileInputStream files = new FileInputStream(f);
		 XSSFWorkbook workbook = new XSSFWorkbook(files);
		 XSSFSheet Sheet1 = workbook.getSheetAt(0);
		 
		 int row = Sheet1.getPhysicalNumberOfRows();

		 for (int i = 1; i < row; i++) {
		 String Name = Sheet1.getRow(i).getCell(0).getStringCellValue();
		 Thread.sleep(5000);
		 driver.findElement(By.id("Name")).sendKeys(Name);
		 WebElement element = driver.findElement(By.id("ParentCategoryId"));
		 Select s = new Select(element);
		 s.selectByIndex(5);
		 driver.findElement(By.name("save")).click();
		 driver.findElement(By.id("SearchCategoryName")).sendKeys(Name);
		 driver.findElement(By.id("search-categories")).click();
		 Thread.sleep(5000);
		 
		 }
		 String Name1 = driver.findElement(By.xpath("//tr[@class='odd']/td[2]")).getText();
		 return Name1;
		
 }   
 /*  public String addCategory(WebDriver driver, String Name, String Description) throws InterruptedException {
	   
	   driver.findElement(By.linkText("Catalog")).click();
	   Thread.sleep(3000);
	   driver.findElement(By.linkText("Categories")).click();
	   driver.findElement(By.linkText("Add new")).click();
	   driver.findElement(By.id("Name")).sendKeys(Name);
	   driver.findElement(By.xpath("//div[@role='menu']")).sendKeys(Description);
	   WebElement parentCategory = driver.findElement(By.id("ParentCategoryId"));
	   
	   Select sel= new Select(parentCategory);
	   sel.selectByVisibleText("Electronics");
	   
	   driver.findElement(By.name("save")).click();
	   
	   driver.findElement(By.id("SearchCategoryName")).sendKeys("Mobile");
	   WebElement searchPublishedId = driver.findElement(By.id("SearchPublishedId"));
	   Select sel1 = new Select(searchPublishedId);
	   sel1.selectByVisibleText("All");
	   
	   driver.findElement(By.id("search-categories")).click();
	   String name = driver.findElement(By.xpath("//tr[@class='odd']/td[2]")).getText();
	   
	   return name;
   }
   */
	 
	 public String manufatureres(WebDriver driver) throws InterruptedException, IOException {
		 
		 //driver.findElement(By.linkText("Catalog")).click();
		 
		 driver.findElement(By.linkText("Manufacturers")).click();
		 driver.findElement(By.linkText("Add new")).click();
		 File f = new File("/home/shrikanth/Documents/Test_data/Assignment.xlsx");
		 FileInputStream files = new FileInputStream(f);
		 XSSFWorkbook workbook = new XSSFWorkbook(files);
		 XSSFSheet Sheet1 = workbook.getSheetAt(0);
		 
		 int row = Sheet1.getPhysicalNumberOfRows();

		 for (int i = 1; i < row; i++) {
		 String Name = Sheet1.getRow(i).getCell(0).getStringCellValue();
		 String Description = Sheet1.getRow(i).getCell(1).getStringCellValue();
		 Thread.sleep(5000);
		 driver.findElement(By.id("Name")).sendKeys(Name);
		 //driver.findElement(By.xpath("//div[@role='menu']")).sendKeys(Description);
		 driver.findElement(By.name("save")).click();
		 driver.findElement(By.id("SearchManufacturerName")).sendKeys(Name);
		
		 WebElement manufrDp = driver.findElement(By.id("SearchPublishedId"));
		 
		 Select sel = new Select(manufrDp);
		 sel.selectByIndex(0);
		 driver.findElement(By.id("search-manufacturers")).click();
		 Thread.sleep(3000);
		 
		 }
		 String Name1 = driver.findElement(By.xpath("//tr[@class='odd'][1]/td[2]")).getText();
		 return Name1;
		
 }   
   
	
	public void logoutApplication(WebDriver driver) {
		driver.findElement(By.linkText("Logout")).click();
	}
	
	
}
