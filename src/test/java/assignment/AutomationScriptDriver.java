package assignment;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class AutomationScriptDriver {
 WebDriver driver=null;
	AutomationScripts script = new AutomationScripts();
	
	//WebDriver driver;
	
	@BeforeClass
	void setUp() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	}
	
	@AfterClass
	void closeApp() {
		driver.quit();
	}
	
	@Test(priority=1)
	public void testLaunch() {
		script.launchApplication(driver);
	}
	
	@Test(priority=2)
	public void testLogin() {
		String actualName = script.loginToApplication(driver);
		String expectedName = "John Smith";
		Assert.assertEquals(actualName, expectedName);
	}
	
	
	@Test(priority=3)
	public void testSearchProduct() {
		String actualProduct = script.searchProduct(driver);
		String expectProduct ="Build your own computer";
		Assert.assertEquals(actualProduct, expectProduct);
	}
	
	@Test(priority=4) 
	public void testAddCategory() throws InterruptedException, IOException {
		String actualop =script.categories(driver);
		String expectedop ="Computers >> Desktops >> iphone >> Mobile";
		Assert.assertEquals(actualop, expectedop);
	}
	
	@Test(priority=5)
	public void testManufatureres() throws InterruptedException, IOException {
		String actualop =script.manufatureres(driver);
		String expectedop ="Mobile";
		Assert.assertEquals(actualop, expectedop);
	}
	@Test(priority=6)
	public void testLogout() {
		script.logoutApplication(driver);
	}
	
}
